# PiNS Propeller Clock - Arduino Source Code

## Required Libraries

All of these can be installed within the Arduino IDE using the Library Manager. 

* TimerOne
* PinChangeInterrupt

## Other instructions

Code has been compiled with -O2 (tells the compiler to optimize for speed). Should make it run more stable, but probably works fine without. 
