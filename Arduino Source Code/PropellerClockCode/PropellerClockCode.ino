#include "stdint.h"
#include "stdbool.h"
#include "EEPROM.h"
#include "TimerOne.h"
#include "PinChangeInterrupt.h"
#include "PinChangeInterruptBoards.h"
#include "PinChangeInterruptPins.h"
#include "PinChangeInterruptSettings.h"
#include "IncrementButton.h"


//
// Pin definitions
//

constexpr uint8_t START_POSITION = 0;
constexpr uint8_t BT_MINS        = 2;
constexpr uint8_t BT_HOURS       = 3;

/*
 * D0 - D7  => PD0 - PD7
 * D8 - D13 => PB0 - PB5
 * A0 - A5  => PC0 - PC5
 * 
 * Mapping of the 16 LEDs to the pins: (lowest bit = outermost LED)
 * 
 * Bit  0 -  3: PD4 - PD7
 * Bit  4 -  9: PB0 - PB5
 * Bit 10 - 15: PC0 - PC5
 */
static constexpr uint8_t BITMASK_D        = 0b11110000;
static constexpr uint8_t BITMASK_D_COUNT  = 4;
static constexpr uint8_t BITMASK_D_OFFSET = 4;
static constexpr uint8_t BITMASK_B        = 0b00111111;
static constexpr uint8_t BITMASK_B_COUNT  = 6;
static constexpr uint8_t BITMASK_B_OFFSET = 0;
static constexpr uint8_t BITMASK_C        = 0b00111111;
static constexpr uint8_t BITMASK_C_COUNT  = 6;
static constexpr uint8_t BITMASK_C_OFFSET = 0;


//
// Button settings
// 

constexpr uint16_t BT_DENOISE_MS = 100;

// For adjusting the time when the clock isn't spinning.
IncrementButton incrementHours(BT_HOURS, false, BT_DENOISE_MS, 500, 1, 1);
IncrementButton incrementMins( BT_MINS,  false, BT_DENOISE_MS, 750, 1, 10);


//
// Serial Settings
// 

constexpr uint16_t SERIAL_TIMEOUT_MS = 200;
bool serialCLIEnabled = false;


// 
// Clock Variables
// 

/* 
 * Notes: 
 *   * Current code versions show such little offset, that it can be adjusted
 *     by bending the wire marking the starting position. With a non-flexible 
 *     marker it might be useful though. Or if the tick rate is lower. 
 */
volatile  int16_t progress = 0;
constexpr int16_t OFFSET   = 0; 
constexpr int16_t TICKS_PER_REV = 120;
volatile uint32_t lastPass = 0;
int32_t secondsUncorrected = 0;

// Stuff for saving the correction values to the EEPROM
constexpr uint8_t DATA_VALID = 42;

struct TimeCorrection
{
  uint8_t dataValid;
  int32_t offsetPer12h;
};

struct TimeCorrection timeCorrection;


//
// Hands and clock design (size and position of everything)
//

// Outer ring of the clock
constexpr uint16_t LED_STATE_ALWAYS_ON = 0b0000000000000001;

// Lines that don't move. 
struct Lines 
{
  const int16_t  DISTANCE;
  const uint16_t BITS;
};

constexpr struct Lines QUARTER_LINES = {.DISTANCE = TICKS_PER_REV /  4, .BITS = 0b0000000000000110};
constexpr struct Lines HOURLY_LINES  = {.DISTANCE = TICKS_PER_REV / 12, .BITS = 0b0000000000000010};


struct Hand
{
  float          value;
  int16_t        position;
  const int16_t  COUNT_PER_REV;
  const uint16_t BITS;
};

volatile struct Hand hours   = {.value = 0.0f, .position = 0, .COUNT_PER_REV = 12, .BITS = 0b1111110000000000};
volatile struct Hand minutes = {.value = 0.0f, .position = 0, .COUNT_PER_REV = 60, .BITS = 0b1111111110000000};
volatile struct Hand seconds = {.value = 0.0f, .position = 0, .COUNT_PER_REV = 60, .BITS = 0b1111111111100000};

const int32_t SECONDS_NOMINAL_PER_12H =   ((int32_t) seconds.COUNT_PER_REV) 
                                        * ((int32_t) minutes.COUNT_PER_REV) 
                                        * ((int32_t) hours.COUNT_PER_REV);


// 
// Code
// 

/*
 * Set all 16 LEDs. each one of the 16 bits of "state" determines
 * the state of one LED. Alternative, much slower version (Note: 
 * pin numbers might need to be adjusted!): 
 *   for (uint8_t i = 0; i < 16; i++)
 *   {
 *     if (state & (1 << i))
 *     {
 *       digitalWrite(i, HIGH);
 *     }
 *     else
 *     {
 *       digitalWrite(i, LOW);
 *     }
 *   }
 */
void setLeds(uint16_t state)
{
  // Select the bits of the led array for the corresponding port
  // Shift them to the right position within the port (..._OFFSET)
  // Discard the selected bits from state (..._COUNT)
  uint8_t d = (state << BITMASK_D_OFFSET) & BITMASK_D;
  state >>= BITMASK_D_COUNT;
  uint8_t b = (state << BITMASK_B_OFFSET) & BITMASK_B;
  state >>= BITMASK_B_COUNT;
  uint8_t c = (state << BITMASK_C_OFFSET) & BITMASK_C;

  // Clear all LEDs
  PORTD &= ~BITMASK_D;
  PORTB &= ~BITMASK_B;
  PORTC &= ~BITMASK_C;

  // Set the new LED states.
  PORTD |=  d;
  PORTB |=  b;
  PORTC |=  c;
}


/*
 * Interrupt service routine that updates the LED state for every
 * position around the circle. Called by a timer interrupt to have
 * precise timing (and thus an even image). 
 */
void update()
{
  uint16_t state = 0;

  if (progress < TICKS_PER_REV)
  {
    int16_t position = progress + OFFSET;

    if (position < 0)
    {
      position += TICKS_PER_REV;
    }
    else if (position >= TICKS_PER_REV)
    {
      position -= TICKS_PER_REV;
    }

    state = LED_STATE_ALWAYS_ON;

    if (position % QUARTER_LINES.DISTANCE == 0)
    {
      state |= QUARTER_LINES.BITS;
    }
    else if (position % HOURLY_LINES.DISTANCE == 0)
    {
      state |= HOURLY_LINES.BITS;
    }

    if (position == hours.position)
    {
      state |= hours.BITS;
    }

    if (position == minutes.position)
    {
      state |= minutes.BITS;
    }

    if (position == seconds.position)
    {
      state |= seconds.BITS;
    }
  }
  else
  {
    Timer1.stop();
  }

  setLeds(state);
  progress++;
}


/*
 * Called by a pin change interrupt of the start position pin. 
 * Checks how fast the clock is spinning and adjusts the timer
 * accordingly. 
 */
void crossedStartPosition()
{
    // Measure the time elapsed since the last rotation
    uint32_t time = micros();
    uint16_t newPeriod = (time - lastPass) / TICKS_PER_REV;
    lastPass = time;

    // micros() sometimes runs slower than it should - probably because
    // its interrupts are delayed by the interrupts from this code.
    // We want to filter these shorter-than-should-be periods out. 
    // Relying on the maximum out of the last X measurement turned out 
    // to give the most stable results. 
    static constexpr uint8_t LAST_PERIODS_COUNT = 32;
    static uint16_t lastPeriods[LAST_PERIODS_COUNT];
    static uint8_t lastPeriodsIndex = 0;

    lastPeriods[lastPeriodsIndex] = newPeriod;
    lastPeriodsIndex++;
    lastPeriodsIndex %= LAST_PERIODS_COUNT;

    uint16_t maxPeriod = 0;
    for (uint8_t i = 0; i < LAST_PERIODS_COUNT; i++)
    {
      uint16_t val = lastPeriods[i];
      if (val > maxPeriod)
      {
        maxPeriod = val;
      }
    }
    
    // Update the Timer1 period accordingly.
    Timer1.setPeriod(maxPeriod);

    progress = 0;
    Timer1.start();
}


/*
 * Display values when the clock is not spinning. See PCB silkscreen
 * to get a better idea. 
 * Used to allow the user to set the clocks time when it's not spinning.
 */
void displayTimeStopped(bool hoursInsteadOfMins, uint8_t value)
{
  constexpr uint16_t UNITS_SHIFT = 2;
  constexpr uint16_t TENS_SHIFT  = UNITS_SHIFT + 9;

  constexpr uint16_t LED_STATE_INDIC_HOURS = 0b0000000000000001;
  constexpr uint16_t LED_STATE_INDIC_MINS  = 0b0000000000000010;

  uint16_t state = 0;

  if (hoursInsteadOfMins)
  {
    state |= LED_STATE_INDIC_HOURS;
  }
  else
  {
    state |= LED_STATE_INDIC_MINS;
  }

  uint16_t tens  = value / 10;
  uint16_t units = value % 10;

  // illuminate corresponding bit
  // Note: bit 0 is actually the indicator for "1", hence the right shift.
  // This only works if units and tens are <=15 - which is the case.
  // Finally shift it to the right progress in the led line.
  tens  = ((1U << tens)  >> 1) << TENS_SHIFT;
  units = ((1U << units) >> 1) << UNITS_SHIFT;

  state |= tens;
  state |= units;

  setLeds(state);
}


/*
 * Based on the seconds counter, determine the position of the seconds, 
 * minutes and hours hand. This also includes the adjustment of the time
 * in case the Arduinos oscillator is off. 
 */
void setHands()
{
    // No need to count more than the 12 hours (= 12 * 60 * 60 seconds) that can be displayed. 
    // The correction is included such that secondsUncorrected flows over after exactly 12h no matter 
    // which value it actually has. 
    // This seemed like the easiest way to correct it while maintaining an accurate time over
    // the entire day (instead of just correcting it once at rollover).
    int32_t secondsUncorrectedPer12h = SECONDS_NOMINAL_PER_12H + timeCorrection.offsetPer12h;    
    
    secondsUncorrected %= secondsUncorrectedPer12h;

    int32_t t = secondsUncorrected * SECONDS_NOMINAL_PER_12H / secondsUncorrectedPer12h;

    hours.value   = (float) t / (seconds.COUNT_PER_REV * minutes.COUNT_PER_REV);
    t %= seconds.COUNT_PER_REV * minutes.COUNT_PER_REV;
    minutes.value = (float) t / (seconds.COUNT_PER_REV);
    t %= seconds.COUNT_PER_REV;
    seconds.value = t;

    hours.position   = hours.value   * TICKS_PER_REV / hours.COUNT_PER_REV;
    minutes.position = minutes.value * TICKS_PER_REV / minutes.COUNT_PER_REV;
    seconds.position = seconds.value * TICKS_PER_REV / seconds.COUNT_PER_REV;
}


/*
 * Check if one of the user adjustment buttons has been pressed. If so, 
 * apply the changes and show it to the user. 
 */
void checkButtons()
{
  uint8_t h = incrementHours.getIncrement();
  uint8_t m = incrementMins.getIncrement();

  if (h || m)
  {
    Timer1.stop();
    m += h * minutes.COUNT_PER_REV;
    secondsUncorrected += m * seconds.COUNT_PER_REV;
    setHands();

    if (h)
    {
      displayTimeStopped(true, hours.value);
    }
    else
    {
      displayTimeStopped(false, minutes.value);
    }
  }
}


/*
 * Increment the seconds counter. This does not include any
 * corrections in case millis() is running too slow or too fast. 
 */
void incrementTime()
{
  static uint32_t nextIncrementMS = 0;
  
  uint32_t timeMS = millis();

  if (timeMS >= nextIncrementMS)
  {
    nextIncrementMS += 1000;
    secondsUncorrected++;
    setHands();
  }
}


/*
 * Check if a command over serial has been received and process it. 
 * Update the config if it's valid. 
 */
void checkSerialConfig()
{
  if (!serialCLIEnabled)
  {
    serialTickInfo();
    return;
  }

  // One digit for the value, one for the \n following it. 
  if (Serial.available() < 2)
  {
    return;
  }

  // Ignore non-numeric chars
  auto c = Serial.peek();
  if (!((c >= '0' && c < '9') || c == '-'))
  {
    // Discard char
    Serial.read();
    return;
  }

  // Something useful has been received over serial. 
  
  // Check if it took longer than the timeout.
  auto t = millis();
  int32_t correction = Serial.parseInt(SKIP_WHITESPACE, "\n");

  // Check if value is in range, otherwise the adjustment math will fail. 
  static const int32_t UPPER_LIMIT = INT32_MAX / SECONDS_NOMINAL_PER_12H * 2;
  static const int32_t LOWER_LIMIT = INT32_MIN / SECONDS_NOMINAL_PER_12H * 2;
  
  if (millis() - t >= (SERIAL_TIMEOUT_MS / 2)
      || correction > UPPER_LIMIT
      || correction < LOWER_LIMIT)
  {
    Serial.println("Error: Could not parse input. ");
    Serial.print("Value range: ");
    Serial.print(LOWER_LIMIT);
    Serial.print(" - ");
    Serial.println(UPPER_LIMIT);
    Serial.flush();
    // Something went wrong. Resend the info message. 
    initSerialConfig();
    return;
  }

  // Discard any remaining bytes in the serial buffer (f.ex. \n after the number)
  while (Serial.available())
  {
    Serial.read();
  }

  // Correction per day (24h) is easier for the user, but the
  // clock actually operates on a 12 hour base (since it can't
  // make a difference between 3 and 15 o'clock anyway). 
  timeCorrection.offsetPer12h = correction / 2;

  // This is pretty much only necessary to find out whether a value had been written
  // to the EEPROM previously
  timeCorrection.dataValid = DATA_VALID;
  
  EEPROM.put(0, timeCorrection);

  
  Serial.print("Saved new value: ");
  Serial.print(correction);
  Serial.println("");
}


/*
 * For debugging purposes and for more convenient testing of the 
 * Arduinos time accuracy, the current time (corrected and uncorrected)
 * is also sent over serial. 
 */
void serialTickInfo()
{
  static auto lastInfo = 0;

  if (lastInfo != secondsUncorrected)
  {
    lastInfo = secondsUncorrected;
    Serial.println("");
    Serial.print("Current time (uncorrected) [s]: ");
    Serial.println(secondsUncorrected);
    Serial.print("Current time (corrected) [s]:   ");
    Serial.print((int32_t) hours.value * 3600 + (int32_t) minutes.value * 60 + (int32_t) seconds.value);
    Serial.print(" = ");
    Serial.print((int32_t) hours.value);
    Serial.print(":");
    Serial.print((int32_t) minutes.value);
    Serial.print(":");
    Serial.println((int32_t) seconds.value);
  }
}

/*
 * Load the config from the EEPROM and check if the serial
 * interface can be used (a.k.a. the RX pin is not blocked). 
 * Currently this is only used for a single parameter, but it
 * could be extended in the future. 
 */
void initSerialConfig()
{
  // Load config from EEPROM
  EEPROM.get(0, timeCorrection);
  if (timeCorrection.dataValid != DATA_VALID)
  {
    // Probably a fresh EEPROM to which no valid data has been written so far. 
    timeCorrection.offsetPer12h = 0;
    timeCorrection.dataValid = DATA_VALID;
  }
  
  // Check if the RX pin is free (not pulled down). Else disable the serial 
  // receiving to prevent erroneuous input. 
  // If the pin is free, disable the associated pin change interrupt since
  // it'll be used as RX pin. 

  // Make sure the pin is set up properly.
  Serial.begin(9600);
  // Serial RX pin is always pin 0
  serialCLIEnabled = digitalRead(0);
  Serial.setTimeout(SERIAL_TIMEOUT_MS);

  Serial.println("");
  if (serialCLIEnabled)
  {
    Serial.println("Serial input enabled. ");
    Serial.println("");

    Serial.println("Enter by how many seconds the time deviates over ");
    Serial.println("one day (24 hours). A positive value means that the ");
    Serial.println("clock has been running faster than it should. ");
  }
  else
  {
    Serial.println("Serial input disabled. "); 
    Serial.println("Please block the optical sensor and reset or power ");
    Serial.println("cycle the Arduino. Alternatively, remove the ");
    Serial.println("Arduino from its socket. ");
  }
  Serial.println("");
  Serial.print("Current correction value [s/24h]: ");
  Serial.println(timeCorrection.offsetPer12h * 2);
  Serial.println("");
}


void setup() 
{
  initSerialConfig();

  // Set the LED pins in bulk as outputs
  DDRD |= BITMASK_D;
  DDRB |= BITMASK_B;
  DDRC |= BITMASK_C;

  // Pin with the optical sensor for the start position. 
  pinMode(START_POSITION, INPUT_PULLUP);

  // To get the most reliable timing, the start position triggers 
  // an interrupt (instead of being polled). 
  // Only enable it if its not already used as serial pin.
  if (!serialCLIEnabled)
  {
    attachPCINT(digitalPinToPCINT(START_POSITION), crossedStartPosition, RISING);
    enablePCINT(digitalPinToPCINT(START_POSITION));
  }

  // Init Timer1 which is responsible for displaying the clock
  // Note that this always runs the ISR once (possibly because of this: 
  //   https://github.com/PaulStoffregen/TimerOne/blob/7aedcec6edbd0e5df9c8123e3f1e1cdf0717b07c/TimerOne.h#L222
  // )
  // Therefore we set "progress" such that it disables itself. 
  progress = TICKS_PER_REV + 1;
  Timer1.initialize();
  Timer1.attachInterrupt(update);

  if (serialCLIEnabled)
  {
    // Function test of the LEDs
    /*for (uint8_t i = 4; i <= 13; i++)
    {
      digitalWrite(i, HIGH);
      delay(200);
      digitalWrite(i,  LOW);
    }
    for (uint8_t i = A0; i <= A5; i++)
    {
      digitalWrite(i, HIGH);
      delay(200);
      digitalWrite(i,  LOW);
    }*/
    for (uint8_t i = 0; i < 16; i++)
    {
      setLeds(1<<i);
      delay(200);
    }
    setLeds(0);
  }
}

void loop() 
{
  checkButtons();

  checkSerialConfig();

  incrementTime();
}
