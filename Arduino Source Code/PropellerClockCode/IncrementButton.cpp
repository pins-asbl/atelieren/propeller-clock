#include "IncrementButton.h"


IncrementButton::IncrementButton(uint8_t pin, bool activeState, uint16_t denoiseMS, uint16_t delayMS, uint8_t stepSmall, uint8_t stepLarge)
{
  this->pin = pin;
  this->activeState = activeState;
  this->denoiseMS = denoiseMS;
  this->delayMS = delayMS;
  this->stepSmall = stepSmall;
  this->stepLarge = stepLarge;
  this->pressed = 0;
  pinMode(pin, INPUT_PULLUP);
}


IncrementButton::~IncrementButton()
{

}

uint8_t IncrementButton::getIncrement()
{
  auto timeMS = millis();
  
  // Serial.println(digitalRead(pin) == activeState);
  if (digitalRead(pin) == activeState)
  {
    if (!pressed)
    {
      pressed = timeMS;
      return stepSmall;
    }
    else if (timeMS - pressed >= delayMS)
    {
      pressed += delayMS;
      return stepLarge;
    }
  }
  else if (timeMS - pressed >= denoiseMS)
  {
    pressed = 0;
  }
  
  return 0;
}