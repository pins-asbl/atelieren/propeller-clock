
#ifndef INCREMENT_BUTTON_H_
#define INCREMENT_BUTTON_H_


#include "stdint.h"
#include "stdbool.h"
#include "Arduino.h"



class IncrementButton
{
  public:
    IncrementButton(uint8_t pin, bool activeState, uint16_t denoiseMS, uint16_t delayMS, uint8_t stepSmall, uint8_t stepLarge);
    virtual ~IncrementButton();
    uint8_t getIncrement();
  private:
    uint8_t pin;
    bool activeState;
    uint16_t denoiseMS;
    uint32_t pressed;
    uint32_t delayMS;
    uint8_t stepSmall;
    uint8_t stepLarge;
};


#endif // INCREMENT_BUTTON_H_
