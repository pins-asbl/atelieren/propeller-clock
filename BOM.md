# PiNS Propeller Clock - Bill of Material

List of all orders that have been placed to get **30** kits. 


### JLCPCB

| Product           | Quantity | Unit Price [EUR] | Total Price [EUR] |
|-------------------|---------:|-----------------:|------------------:|
| PCB Base          |       30 |           0,5027 |             15,08 |
| PCB Motor         |       30 |           0,4322 |             12,97 |
| PCB Propeller     |       30 |           0,3954 |             11,86 |
| PCB Support       |       30 |           0,2360 |              7,08 |
| PCB Support Foot  |       30 |           0,1747 |              5,24 |
|                   |          |                  |                   |
| Subtotal          |          |                  |             52,23 |
|                   |          |                  |                   |
| Shipping (FedEx)  |          |          27,6800 |             27,68 |
| Customs (Prepaid) |          |          12,0600 |             12,06 |
| Discount          |          |         - 4,6000 |            - 4,60 |
|                   |          |                  |                   |
| Total             |          |          87,3700 |             87,37 |


### LCSC

| Product                                                                                                                                     | Quantity | Unit Price [USD] | Total Price [USD] |
|---------------------------------------------------------------------------------------------------------------------------------------------|---------:|-----------------:|------------------:|
| C410679<br>RN 1/4W 150R F T/B A1<br>Metal Film Resistors 150Ω 250mW ±50ppm/℃ ±1% Plugin Through Hole Resistors ROHS                         |      500 |           0,0070 |          3,50     |
| C52833<br>ITR9608<br>DIP-4 Photointerrupters - Slot Type - Transistor Output ROHS                                                           |       30 |           0,1368 |          4,10     |
| C668587<br>MICRO XNJ ZB<br>USB 2.0 1.8A 1 Surface Mount 5P Female -30℃~+80℃ Micro-B SMD USB Connectors ROHS                                 |       30 |           0,0371 |          1,11     |
| C431487<br>16 YXA 220MEFC(6.3X11)<br>220uF 16V ±20% 220mA@120Hz Plugin,D6.3xL11mm Aluminum Electrolytic Capacitors - Leaded ROHS            |       30 |           0,0540 |          1,62     |
| C105874<br>RC0805JR-075K1L<br>125mW Thick Film Resistors ±100ppm/℃ ±5% 5.1kΩ 0805 Chip Resistor - Surface Mount ROHS                        |      100 |           0,0018 |          0,18     |
| C2897378<br>PM254-1-15-Z-8.5<br>2.54mm Plugin 3A 15P -40℃~+105℃ Straight 1x15P 8.5mm Top Square Holes Plugin,P=2.54mm Female Headers ROHS   |       60 |           0,1804 |         10,82     |
| C428628<br>1825910-6<br>50mA Straight SPST 160gf 24V Plugin Tactile Switches ROHS                                                           |       60 |           0,0594 |          3,56     |
| C914821<br>MFR-25FTF52-1K<br>Metal Film Resistors 1kΩ 250mW ±100ppm/℃ ±1% Plugin Through Hole Resistors ROHS                                |       50 |           0,0109 |          0,55     |
| C172993<br>MFR-25FTE52-270R<br>Metal Film Resistors 270Ω 250mW ±50ppm/℃ ±1% Plugin Through Hole Resistors ROHS                              |      150 |           0,0118 |          1,77     |
|                                                                                                                                             |          |                  |                   |
| Subtotal                                                                                                                                    |          |                  |         27,21     |
|                                                                                                                                             |          |                  |                   |
| Shipping (DHL Express)                                                                                                                      |          |                  |         37,94     |
| Customs (paid locally) [EUR]                                                                                                                |          |                  |         29,60 EUR |
|                                                                                                                                             |          |                  |                   |
| Total [EUR]                                                                                                                                 |          |                  |         90,31 EUR |


### Aliexpress

| Product                                                                                                                                     | Quantity | Unit Price [EUR] | Total Price [EUR] |
|---------------------------------------------------------------------------------------------------------------------------------------------|---------:|-----------------:|------------------:|
| 5V2A Wireless Power Supply Module Wireless Charger Module Transmitter Receiver Terminal Circuit Board Module for DIY Phone                  |       30 |           5,0163 |            150,49 |
|                                                                                                                                             |          |                  |                   |
| Shipping including customs (Aliexpress Standard 12 day)                                                                                     |          |                  |              7,96 |
|                                                                                                                                             |          |                  |                   |
| Total                                                                                                                                       |          |                  |            158,45 |

| Product                                                                                                                                     | Quantity | Unit Price [EUR] | Total Price [EUR] |
|---------------------------------------------------------------------------------------------------------------------------------------------|---------:|-----------------:|------------------:|
| Micro USB Nano 3.0 With the bootloader compatible Nano controller for arduino CH340 USB driver 16Mhz ATMEGA328P                             |       30 |           2,3767 |             71,30 |
|                                                                                                                                             |          |                  |                   |
| Shipping including customs (Aliexpress Standard 12 day)                                                                                     |          |                  |              9,20 |
|                                                                                                                                             |          |                  |                   |
| Total                                                                                                                                       |          |                  |             80,50 |


### Pollin

| Product                                                                                     | Quantity | Unit Price [EUR] | Total Price [EUR] |
|---------------------------------------------------------------------------------------------|---------:|-----------------:|------------------:|
| Gerätefuß, 8x3mm, selbstklebend, 10St.                                                      |       10 |             0,60 |              6,00 |
| QUADRIOS LED, 5 mm, Lichtfarbe: Blau, Gehäusefarbe: Diffus-Blau, 1300 mcd, 460 nm, 2,54 mm  |      150 |             0,05 |              7,50 |
| QUADRIOS LED, 5 mm, Lichtfarbe: Gelb, Gehäusefarbe: Diffus-Gelb, 700 mcd, 590 nm, 2,54 mm   |      150 |             0,05 |              7,50 |
| QUADRIOS LED, 5 mm, Lichtfarbe: Grün, Gehäusefarbe: Diffus-Grün, 2500 mcd, 520 nm, 2,54 mm  |      150 |             0,05 |              7,50 |
| QUADRIOS, 1802O024, LED Sortiment 5 mm Leuchtdioden Rot 100 tlg                             |        2 |             5,60 |             11,20 |
| JOHNSON Gleichstrommotor 30037                                                              |       30 |             0,99 |             29,70 |
|                                                                                             |          |                  |                   |
| Subtotal                                                                                    |          |                  |             69,40 |
|                                                                                             |          |                  |                   |
| Shipping (DHL)                                                                              |          |                  |              5,95 |
| Shipping (Post Packup Import)                                                               |          |                  |              6,39 |
| Discount                                                                                    |          |                  |             -5,00 |
|                                                                                             |          |                  |                   |
| Total                                                                                       |          |                  |             76,74 |


### Reichelt

| Product                                | Quantity | Unit Price [EUR] | Total Price [EUR] |
|----------------------------------------|---------:|-----------------:|------------------:|
| SS X22200 Schiebeschalter 2x Ein - Ein |       30 |             0,44 |             13,15 |
|                                        |          |                  |                   |
| Subtotal                               |          |                  |             13,15 |
|                                        |          |                  |                   |
| Shipping (DPD)                         |          |                  |              6,95 |
|                                        |          |                  |                   |
| Total                                  |          |                  |             20,10 |


### Hornbach

| Product                                                                            | Quantity | Unit Price [EUR] | Total Price [EUR] |
|------------------------------------------------------------------------------------|---------:|-----------------:|------------------:|
| Zylinderschraube m. Schlitz DIN 84 M2,5x16 mm galv. verzinkt, 100 Stück            |        1 |             2,53 |              2,53 |
| Linsenkopfschraube 4.8 m. Kreuzschlitz DIN 7985 M3x20 mm galv. verzinkt, 100 Stück |        1 |             3,72 |              3,72 |
| Kotflügelscheibe 4,3 x 15 mm galv.verzinkt, 100 Stück                              |        2 |             3,02 |              3,02 |
| Unterlegscheibe DIN 9021, 3,2 mm galv.verzinkt, 100 Stück                          |        1 |             1,44 |              3,02 |
| Sechskantmutter DIN 934 M3 galv. verzinkt, 100 Stück                               |        1 |             1,74 |              1,74 |
|                                                                                    |          |                  |                   |
| Total                                                                              |          |                  |             11,01 |


### 3D Printing

Filament used:

| Filament             | Spool Size | Price |
|----------------------|-----------:|------:|
| Eryone PLA Jet Black |     1,00kg | 26,95 |
| Prusament PC Blend   |     0,97kg | 49,90 |

Part cost (based on filament consumption):

| Part              | Material | Quantity | Unit Price [EUR] | Total Price [EUR] |
|-------------------|----------|---------:|-----------------:|------------------:|
| WP TX Coil Mount  | PLA      |       30 | 0,23             |              6,90 |
| WP TX Coil Clip   | PLA      |       30 | 0,08             |              2,40 |
| WP TX PCB Mount   | PLA      |       30 | 0,05             |              1,50 |
| WP RX Coil Clip   | PLA      |       60 | 0,09             |              5,40 |
| WP RX PCB Mount   | PLA      |       30 | 0,09             |              2,70 |
| PCB Motor Adapter | PC Blend |       30 | 0,10             |              3,00 |
|                   |          |          |                  |                   |
| Total             |          |          |                  |             21,90 |


### Total

| Type     | EUR    |
|----------|-------:|
| Goods    | 402,65 |
| Shipping | 102,07 | 
| Customs  |  41,66 |
|          |        |
| Total    | 546,38 |

=> 18,21€/kit
