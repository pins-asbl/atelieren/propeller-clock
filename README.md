# PiNS Propeller Clock

## Overview

![Clock at rest](Pictures/2023-08-19 17.33.54%201179.jpg)

![Clock spinning](Pictures/2023-08-19 19.53.03%201198.jpg)

![iBom Preview](Schematic%20and%20PCB/iBom/PropellerClockPCB-v1.1.0-iBom-Preview.png)

## Assembly

* Solder motor PCB components
* Connect all PCBs except for the propeller PCB
* Mount the motor
* Mount the TX inductor
* Solder the propeller PCB components. Careful selecting the right LED resistors (min. 270R for the bright green LEDs)
* Mount the RX inductor
* Balance propeller
* Mount propeller and test. 

## Instructions

### Setting the Time

- Switch the motor off using the big switch at the base PCB. Do not unplug the clock. 
- Use the `HOURS` and `MINUTES` buttons on the propeller to adjust the time. Long press for bigger adjustments. 
- The first two LEDs indicate whether you're adjusting hours or minutes. The next nine LEDs indicate the digits, and the remaining ones the tens. If no other LED except for the `HRS` (or `MIN`) LED is on, it means that the hours (or minutes) are set to zero. 
  ```
  HRS MIN | 1  2  3  4  5  6  7  8  9 | 10 20 30 40 50
   *        *                                           
  => Hours set to 1
  
  HRS MIN | 1  2  3  4  5  6  7  8  9 | 10 20 30 40 50
       *                   *                      *     
  => Minutes set to 46
  
  HRS MIN | 1  2  3  4  5  6  7  8  9 | 10 20 30 40 50
   *                                                    
  => Hours set to 0
  ```
- To leave this menu simply power up the motor and the clock will display the new time. 

### Adjusting Drift

The clock might start drifting over time, running either too slow or too fast. This can be adjusted. 

- You need a serial monitor / console. You can use the [Arduino IDE](https://www.arduino.cc/en/software)
- Connect the Arduino from the propeller to your computer and open a serial console (at 9600baud/s).  
- In the terminal you'll see the instructions on how to do the adjustment. If not, press the reset button on the Arduino once. 
- You shouldn't need to remove the Arduino from its socket. However, if you're having issues, you may want to try it anyway. 

### Flashing the Arduino

The source code is available here. Feel free to play with it! The readme in the folder lists the necessary libraries. You shouldn't need to remove the Arduino from its socket. Just block the optical switch. However, if the connection fails, you might want to remove it anyway. 
