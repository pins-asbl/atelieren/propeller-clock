# PiNS Propeller Clock - CAD Model


## Overview

This folder - and the model itself - is still quite messy. 

## Structure

Rough workflow/structure:

- Designed the propeller PCB in KiCAD, made it as compact as possible. 
- Export as STEP, import into Solidworks
- Designed the other parts around it
- Create drawings for their shapes, export those as DXF
- Import DXF shapes into KiCAD as board outlines
- Create footprints for PCB-PCB connections
- Place remaining components, align where necessary. 

As you can see, the poor integration between Solidworks and KiCAD makes the process rather difficult. It is especially hard to make certain changes, as it requires a lot of manual rework. Even the parts that could be designed in a parametric way mostly aren't. It is mostly a "quick-and-dirty" design and should probably be redone from scratch in a clean way. 

## Comments

### 3D Printing Material Choices / PCB Motor Adapter

Check out this nice material overview: [Prusa Material Table](https://help.prusa3d.com/materials)

PLA is cheap, easy to print, and rather stiff. Makes it a great choice for generic parts and parts that "snap" together. Arguably, the parts with the thin clips should be printed in a stlightly sturdier material that doesn't break as easily but it seems to be fine and keeps things simple. 

The PCB Motor Adapter can be printed in PLA, too. However, after a few hours of operation, the heat from the motor (through its shaft) makes the part warm enough that it gets (too) soft. This combined with the fact that because of the uneven mass distribution, the propeller wants to tilt causes the part to deform. The propeller loses balance and the clock can't be operated anymore. Better balancing is quite difficult - especially without major design changes (see below). 

I therefore opted to print this part in a more heat resistant material. I had PETG, ASA and PC Blend as choices. PETG and ASA are not as stiff as PLA though. At least PETG also tends to slowly deform under permanent load (see Balancing below). This might not have been an issue or could have been compensated by design changes but neither did I feel like making such changes nor did I want to take any risk and have it fail after 10 hours instead of 2. 

Note: The tolerances around the motor shaft are very sensitive. 0.05mm make the difference between doesn't fit and sits firmly. Switching material requires adjusting the tolerances. The SolidWorks Model includes both the PLA and the PC Blend version. The printing files only contain the PC Blend version. 

### Balancing

When designing the propeller I only thought about balancing both sides of the propeller to equal weight. I forgot that both sides of the PCB should be balanced as well. If not, the uneven masses will constantly try to flex the PCB. This (probably) yields in speed-dependent vibrations and higher requirements on the overall sturdiness. If the clock is not stable enough, it can switch from rather low vibrations to out-of-control vibrations. It also places a permanent load on the PCB Motor Adapter as well as the motor bearings. 

Balancing front and back of the PCB isn't easy and probably requires accelerometer measurements. Luckily it is just not bad enough to be a severe issue in this design. For testing purposes, the SolidWorks model includes an additional 3D printed piece that gives more flexibility for placing the balancing weights. 

Future iterations should place the hole for the balancing screw in a place that allows balancing on either side (currently only one side due to the buttons next to it). 
