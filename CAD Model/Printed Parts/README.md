# Print Files

## Printer 

Prusa i3 MK3S. All tolerances have been adjusted to work with this printer and the given printing profiles. YMMV.

## Materials

Everything in (generic) black PLA except for the PCB-Motor-Adapter which is printed in Prusa PC Blend. This part is also pretty much invisible so color doesn't matter as much here. 

See CAD Model Readme for the reasons why it's printed in PC Blend. 

## Files

- STL files exported from SolidWorks (only the parts that need to be printed)
- 3mf files (PrusaSlicer Projects) to print parts for 30 kits
- gcode file exported from each PrusaSlicer Project
- One 3mf file with all the parts for a single set (for protoyping). No separate file for different materials though. 
